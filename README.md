Fairplay bot for AI Nibbles
=========================

AI implementation for [AI Nibbles] (https://github.com/MattiLehtinen/ai-nibbles-server) game.

Installation
------------
    
    git clone https://bitbucket.org/anttik/ai-nibbles-fairplay.git
    npm install
    
Running
-------
    
    node ./app.js <host> <port> <name>
 
 For example:
 
    node ./app.js localhost 6969 Foo
                  

