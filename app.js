var _ = require('lodash');
	pf = require('pathfinding'),
    net = require("net"),
    JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];

// Randomize name to be able to launch multiple instances of the same player
var name = process.argv[4];

var width = null,
    height = null,
    playerNo = null,
	enemyNos = [],
    appleX = -1,
    appleY = -1,
	x = -1,
	y = -1,
	enemyX = -1,
	enemyY = -1,
	direction = null,
	finder = new pf.BestFirstFinder(),
	emptyGrid, nextGrid, nextEvasiveDirection = null;


var evasiveMoves = [
	{ xDiff:  0, yDiff: -1, turnDir: 3, altTurnDir: 4, nextTurnDir: 2 },
	{ xDiff:  0, yDiff:  1, turnDir: 4, altTurnDir: 3, nextTurnDir: 1 },
	{ xDiff: -1, yDiff:  0, turnDir: 1, altTurnDir: 2, nextTurnDir: 4 },
	{ xDiff:  1, yDiff:  0, turnDir: 2, altTurnDir: 1, nextTurnDir: 3 }
];
	
client = net.connect(serverPort, serverHost, function() {
    return send({
        msg: "join",
        data: { player: { name: name } }
    });
});

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {

    if (data.msg === 'start') {

        width = data.data.level.width;
        height = data.data.level.height;
        playerNo = _.findIndex(data.data.players, {name: name});
		emptyGrid = new pf.Grid(width, height);
		nextGrid = emptyGrid.clone();
		
		for( var i = 0; i < data.data.players.length; ++i ) {
			if( i != playerNo ) enemyNos.push(i);
		}
		
    } else if (data.msg === 'positions') {
		
		var snake = data.data.snakes[playerNo];
        x = snake.body[0][0];
        y = snake.body[0][1];
		
		// get current direction
		direction = snake.direction;
	
		// mark own body as "wall"
		snake.body = snake.body.slice(1);
		_.forEach( snake.body, function(bodyPart) {
			nextGrid.setWalkableAt(bodyPart[0], bodyPart[1], false);
		});
		
		// handle each enemy
		var minEnemyDistToApple = Number.MAX_VALUE;
		_.forEach( enemyNos, function(enemyNo) {

			var enemy = data.data.snakes[enemyNo];
			enemyX = enemy.body[0][0];
			enemyY = enemy.body[0][1];
		
			// mark enemy bodies as "wall"
			if(!nextGrid.isWalkableAt(enemyX, enemyY)) enemy.body = enemy.body.slice(1);
			_.forEach( enemy.body, function(bodyPart) {
				nextGrid.setWalkableAt(bodyPart[0], bodyPart[1], false);
			});
			
			// mark enemy's possible next moves as "wall"
			if(nextGrid.isWalkableAt(enemyX+1, enemyY)) nextGrid.setWalkableAt(enemyX+1, enemyY, false);
			if(nextGrid.isWalkableAt(enemyX-1, enemyY)) nextGrid.setWalkableAt(enemyX-1, enemyY, false);
			if(nextGrid.isWalkableAt(enemyX, enemyY+1)) nextGrid.setWalkableAt(enemyX, enemyY+1, false);
			if(nextGrid.isWalkableAt(enemyX, enemyY-1)) nextGrid.setWalkableAt(enemyX, enemyY-1, false);

			minEnemyDistToApple = _.min([minEnemyDistToApple, calcDist(enemyX, enemyY, appleX, appleY)]);
			
		});
		
		if(secondEvasiveMove()) return;

		var path = finder.findPath(x, y, appleX, appleY, nextGrid);
		
		// oops! there is now way out of here
		if(!path[1]) {
		
			evasiveMove(x, y);
			nextGrid = emptyGrid.clone();
			return;
		}
		
		// change direction to follow the path
		if( path[1][0] > x ) sendDirection(4);
		else if( path[1][0] < x ) sendDirection(3);
		else if( path[1][1] < y ) sendDirection(1);
		else if( path[1][1] > y ) sendDirection(2);
		
		nextGrid = emptyGrid.clone();
		
    } else if (data.msg === 'apple') {
        appleX = data.data[0];
        appleY = data.data[1];
    }
});

function calcFreeDists(x, y) {

	var freeDists = [];
	for( var dir = 0; dir < evasiveMoves.length; ++dir) {
		
		var dist = -1;
		var nextX = x, nextY = y, xDiff = evasiveMoves[dir].xDiff, yDiff = evasiveMoves[dir].yDiff;
		do {
			
			nextX += xDiff;
			nextY += yDiff;
			dist++;
						
		} while( nextGrid.isWalkableAt( nextX, nextY ));
		
		freeDists.push(dist);
	}
	
	return freeDists;
}

function evasiveMove(x, y) {

	var directionIndex = direction - 1;
	var evasiveMove = evasiveMoves[directionIndex];
	var freeDists = calcFreeDists(x, y);
	
	var freeDistFront = freeDists[directionIndex];
	var freeDistTurn = freeDists[evasiveMove.turnDir - 1];
	var freeDistAltTurn = freeDists[evasiveMove.altTurnDir - 1];
	
	if( freeDistFront <= 2 && (freeDistTurn > 0 || freeDistAltTurn > 0)) {
		
		if(  freeDistTurn > freeDistAltTurn ) {
			sendDirection(evasiveMove.turnDir);
		} else {
			sendDirection(evasiveMove.altTurnDir);
		}
		
		nextEvasiveDirection = evasiveMove.nextTurnDir;
	}
}

function secondEvasiveMove() {

	if( nextEvasiveDirection == null ) return false;
	
	var evasiveMove = evasiveMoves[nextEvasiveDirection - 1];
	if( nextGrid.isWalkableAt( x + evasiveMove.xDiff, y + evasiveMove.yDiff ) ) {
		sendDirection(nextEvasiveDirection);
		nextEvasiveDirection = null;
		return true;
	}
	
	nextEvasiveDirection = null;
	return false;
	
}

function calcDist(x1, y1, x2, y2) {

	var dist = 0;
	if( x1 > x2 ) dist += x1 - x2; else dist += x2 - x1;
	if( y1 > y2 ) dist += y1 - y2; else dist += y2 - y1;
	return dist;
}

jsonStream.on('error', function() {
    return console.log("disconnected");
});

function sendDirection(pDirection) {
	if( direction != pDirection ) {
		send({msg: 'control', data: {direction: pDirection}});
	}
}

function send(json) {
    var jsonString = JSON.stringify(json);
    console.log("SEND: " + jsonString);
    return client.write(jsonString);
}

